import requests
import json
import os

AlgoList = "Ethash,KawPOW,EtcHash,VerusHash,RandomX,Autolykos2" #@param {type:'string'}
BTCWallet = "3Qd2DPmnVFfEXSCBaKcijVsqS5wkt9EGSM" #@param {type:'string'}
ETCWallet = "0xcd1cbe893acba1f50e71e750c1308e5bdfa0fee5" #@param {type:'string'}
ETHWallet = "0xcd1cbe893acba1f50e71e750c1308e5bdfa0fee5" #@param {type:'string'}
VerusWallet = "RTxV5jDjpndt1BXa8KyZ2rpbZGSfnvgJjs" #@param {type:'string'}
PoolList = "2Miners,luckpool,Nicehash" #@param {type:'string'}
MinerList = "lolminer, Nanominer, Gminer,Trex" #@param {type:'string'}
RBMinerKey = "d7d76571-7dc0-404c-8b9d-e30fe52fe992" #@param {type:'string'}


url = "https://bitbucket.org/jokoGendeng/gendenganyaran/raw/master/setup.json" #Jangan diubah
r = requests.get(url)
data_json = r.text
filename = 'setup.json'
with open(filename,'w') as fd:
    fd.write(data_json)
with open(filename, 'r') as f:
    data = json.load(f)
    data["Config"]["MinerName"] = MinerList
    data["Config"]["MinerStatusKey"] = RBMinerKey
    data["Config"]["PoolName"] = PoolList
    data["Config"]["Wallet"] = BTCWallet
    data["Coins"]["BTC"]["Wallet"] = BTCWallet
    data["Coins"]["ETC"]["Wallet"] = ETCWallet
    data["Coins"]["ETH"]["Wallet"] = ETHWallet
    data["Coins"]["VRSC"]["Wallet"] = VerusWallet
    data["Pools"]["NiceHash"]["BTC"] = BTCWallet
    data["Config"]["Algorithm"] = AlgoList

os.remove(filename)
with open(filename, 'w') as f:
    json.dump(data, f, indent=4)